program main
    use :: Task
    implicit none
    integer i, array_size, number_intervals
    real(8) argument, step
    real(8), dimension(:), allocatable :: x_array, y_array
    
    array_size = 10
    number_intervals = 1000
    allocate(x_array(array_size), y_array(array_size))
    x_array = (/(dble(i)/dble(array_size), i=1,array_size)/)
    y_array = (/(fun(x_array(i)), i=1,array_size)/)
    open(unit = 100, file = 'spline.res')
    step = (x_array(array_size) - x_array(1)) / dble(number_intervals)
    do i = 1, number_intervals
      argument = x_array(1) + step * i
      write(100,*) argument, fun(argument), GetSplineValue(argument, x_array, y_array)
    enddo
    close(100)
    write(6,*) "Spline interpolation result printed in spline.res"
    write(6,*) "Spline Integral: ", GetSplineIntegral(0.2d0, 0.7d0, x_array, y_array)

    contains
      function fun(x_array)
        real(8), intent(in) :: x_array
        real(8) :: fun
        fun = exp(sin(10d0 * x_array)) + 2d0
      end function fun
end program

module Task
  use :: omp_lib
  implicit none
  type sub_matrix
    integer :: x1, x2, y1, y2
    real(8) :: elements_sum
  end type 
  
  intrinsic :: max
  interface max
    procedure choose_max
  end interface
  !$omp declare reduction (max : sub_matrix : omp_out = max(omp_in, omp_out))
        
  contains
  function choose_max(a1, a2) result(res)
    type(sub_matrix), intent(in) :: a1, a2
    type(sub_matrix)             :: res
    
    if(a1%elements_sum > a2%elements_sum) then
      res%x1 = a1%x1; res%x2 = a1%x2
      res%y1 = a1%y1; res%y2 = a1%y2
      res%elements_sum = a1%elements_sum
    else 
      res%x1 = a2%x1; res%x2 = a2%x2
      res%y1 = a2%y1; res%y2 = a2%y2
      res%elements_sum = a2%elements_sum
    endif
  end function choose_max
  
  subroutine GetMaxCoordinates(A, x1, y1, x2, y2)
    implicit none
    real(8), intent(in), dimension(:,:) :: A
    integer(4), intent(out) :: x1, y1, x2, y2
    integer(4) :: n, L, R, Up, Down, m, tmp
    real(8), allocatable :: current_column(:)
    real(8) :: current_sum, max_sum
    type(sub_matrix) :: sub_mat

    m = size(A, dim=1) 
    n = size(A, dim=2) 
    
    sub_mat%x1 = 1
    sub_mat%x2 = 1
    sub_mat%y1 = 1
    sub_mat%y2 = 1
    sub_mat%elements_sum = A(1,1)
    !$omp parallel reduction(max : sub_mat) shared(A, n, m, x1, x2, y1, y2) & 
                   private(L, R, current_column, current_sum, Up, Down)
    allocate(current_column(m))
    !$omp do schedule(dynamic)
    do L = 1, n
      current_column = A(:, L)
      do R = L, n
        if (R > L) then
          current_column = current_column + A(:, R)
        endif 
        call FindMaxInArray(current_column, current_sum, Up, Down)
        if (current_sum > sub_mat%elements_sum) then
          sub_mat%elements_sum = current_sum
          sub_mat%x1 = Up
          sub_mat%x2 = Down
          sub_mat%y1 = L
          sub_mat%y2 = R
        endif
      end do
    end do
    !$omp end do
    deallocate(current_column)
    !$omp end parallel
    x1 = sub_mat%x1
    x2 = sub_mat%x2
    y1 = sub_mat%y1
    y2 = sub_mat%y2
  end subroutine

  subroutine FindMaxInArray(A, Summ, Up, Down)
    implicit none
    real(8), intent(in), dimension(:) :: A
    integer(4), intent(out) :: Up, Down
    real(8), intent(out) :: Summ
    real(8) :: cur_sum
    integer(4) :: minus_pos, i

    Summ = A(1)
    Up = 1
    Down = 1
    cur_sum = 0
    minus_pos = 0

    do i=1, size(A)
      cur_sum = cur_sum + A(i)
      if (cur_sum > Summ) then
        Summ = cur_sum
        Up = minus_pos + 1
        Down = i
      endif
     
      if (cur_sum < 0) then
        cur_sum = 0
        minus_pos = i
      endif
    enddo

  end subroutine
end module

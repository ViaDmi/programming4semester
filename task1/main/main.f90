program main
    use :: omp_lib
    use :: Task
    implicit none
    integer :: n, row
    integer :: x1, x2, y1, y2
    real(8) :: startTime, endTime
    real(8), allocatable :: A(:,:)
    
    allocate(A(1000,1000))
    call random_number(A)
    A = A - 0.5d0

    call omp_set_num_threads(50)
    startTime = omp_get_wtime()
    call GetMaxCoordinates(A, x1, y1, x2, y2)
    endTime = omp_get_wtime()
    
    write(*,*) 'Coordinates of maximum submatrix:'
    write(*,*) x1, y1
    write(*,*) x2, y2
    write(*,*) 'Time: ', endTime - startTime
    
end program
